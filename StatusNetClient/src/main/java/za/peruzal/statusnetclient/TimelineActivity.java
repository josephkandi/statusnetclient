package za.peruzal.statusnetclient;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Administrator on 8/7/13.
 */
public class TimelineActivity extends BaseActivity {
    private static final String TAG = TimelineActivity.class.getSimpleName();
    static final String SEND_TIMELINE_NOTIFICATIONS = "za.peruzal.statusnetclient.SEND_TIMELINE_NOTIFICATIONS";
    StatusData statusData;
    SQLiteDatabase db;
    Cursor cursor;
    //SimpleCursorAdapter adapter;
    TimelineAdapter adapter;
    TimelineReceiver receiver;
    final static  IntentFilter filter =  new IntentFilter(UpdaterService.NEW_STATUS_INTENT);

    static final SimpleCursorAdapter.ViewBinder VIEW_BINDER = new SimpleCursorAdapter.ViewBinder(){
        @Override
        public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
            if (view.getId() != R.id.textCreatedAt)
            return false;

            //Update the created text to relative time
            long timestamp = cursor.getLong(columnIndex);
            CharSequence realTime = DateUtils.getRelativeTimeSpanString(view.getContext(), timestamp);
            ((TextView)view).setText(realTime);
            return true;
        }
    };

    static final String[] FROM = { StatusData.C_CREATED_AT, StatusData.C_USER, StatusData.C_TEXT };
    static final int[] TO = { R.id.textCreatedAt, R.id.textUser, R.id.textText };
    //TextView textTimeline;
    ListView listTimeline;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeline);

        //Find our views
        //textTimeline = (TextView) findViewById(R.id.textTimeline);
        listTimeline = (ListView)findViewById(R.id.listTimeline);

        //Connect to the database
        statusData = ((StatusNetApplication)getApplication()).getStatusData();

        StatusNetApplication app = (StatusNetApplication)getApplication();
        if (app.prefs.getString("username", null) == null){
            startActivity(new Intent(this, PrefsActivity.class));
            Toast.makeText(this,
                    R.string.msg_setup_prefs, Toast.LENGTH_SHORT).show();
        }

        receiver = new TimelineReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.setupList();
        registerReceiver(receiver, filter, SEND_TIMELINE_NOTIFICATIONS, null);
    }

    private void setupList(){
        //Get the data
        cursor = app.getStatusData().getStatusUpdates();

        startManagingCursor(cursor);

        //Setup the adapter
        adapter = new TimelineAdapter(this, cursor);
        adapter.setViewBinder(VIEW_BINDER);

        listTimeline.setAdapter(adapter);

    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //Close the database
        statusData.close();
    }
    class TimelineReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {
            cursor.requery();
            adapter.notifyDataSetChanged();
            Log.d(TAG, "OnReceived");
        }
    }
}