package za.peruzal.statusnetclient;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Administrator on 8/8/13.
 */
class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = DBHelper.class.getSimpleName();
    public DBHelper(Context context) {
        super(context, StatusData.DB_NAME  , null, StatusData.DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE table " + StatusData.TABLE + "(" + StatusData.C_ID + " int primary key, " +
                StatusData.C_CREATED_AT + " int," + StatusData.C_SOURCE +"  text, "+ StatusData.C_USER + " text,  "+ StatusData.C_TEXT +" text)";
        try {
            db.execSQL(sql);
        } catch (SQLException e) {
            Log.e(TAG, "Could not create database", e);
        }
        Log.d(TAG, "onCreated sql " + sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + StatusData.TABLE);
        Log.d(TAG, "onUpdated");
        onCreate(db);
    }
}