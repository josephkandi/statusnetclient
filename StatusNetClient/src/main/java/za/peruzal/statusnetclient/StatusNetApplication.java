package za.peruzal.statusnetclient;

import android.app.Application;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import winterwell.jtwitter.Status;
import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;

/**
 * Created by Administrator on 8/6/13.
 */
public class StatusNetApplication extends Application implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG =StatusNetApplication.class.getSimpleName();
    Twitter twitter;
    SharedPreferences prefs;

    public StatusData getStatusData() {
        return statusData;
    }

    private StatusData statusData;

    public boolean isServiceRunning() {
        return serviceRunning;
    }

    //Connects to the online service and puts the latest statutes into DB
    //Returns the count of new statues
    public synchronized int fetchStatusUpdates(){
        Log.d(TAG, "Fetching status updates");
        Twitter twitter = this.getTwitter();

        if (twitter == null){
            Log.d(TAG, "Twitter connection info not initialized");
            return 0;
        }

        try {
            List<Status> statusUpdates = twitter.getFriendsTimeline();

            Log.d(TAG, statusUpdates.toString());

            long latestStatusCreatedAtTime = this.getStatusData().getLatestStatusCreatedAtTime();
            int count = 0;

            ContentValues values = new ContentValues();
            for (Status status : statusUpdates){
                values.put(StatusData.C_ID, String.valueOf(status.getId()));

                long createdAt = status.getCreatedAt().getTime();
                values.put(StatusData.C_CREATED_AT, createdAt);
                values.put(StatusData.C_SOURCE, status.source);
                values.put(StatusData.C_TEXT, status.text);
                values.put(StatusData.C_USER, status.user.name);

                Log.d(TAG, "Goit update with id " + status.getId() + ". Saving");

                //this.getStatusData().insertOrIgnore(values);
                getContentResolver().insert(StatusProvider.CONTENT_URI, values);

                if (latestStatusCreatedAtTime < createdAt){
                    count++;
                }
            }
                Log.d(TAG, count > 0 ? "Got " + count + " status updates " : "No new status updates");
                return count;

        } catch (RuntimeException e) {
            Log.e(TAG, "Failed to fetch status updates", e);
            return 0;
        }
    }

    public void setServiceRunning(boolean serviceRunning) {
        this.serviceRunning = serviceRunning;
    }

    private boolean serviceRunning;

    public synchronized Twitter getTwitter() {

        if (this.twitter == null){
             String username = prefs.getString("username", "student");
             String password = prefs.getString("password", "password");
             String api_root_url  = prefs.getString("api_root", "http://yamba.peruzal.co.za/api") ;



            if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(api_root_url)){
                twitter = new Twitter(username, password);
                twitter.setAPIRootUrl(api_root_url);
            }
        }
        return twitter;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.prefs.registerOnSharedPreferenceChangeListener(this);
        this.statusData = new StatusData(this);
        Log.d(TAG, "Application onCreated");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.i(TAG, "applicattion onTerminated");
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        twitter = null;
    }
}
