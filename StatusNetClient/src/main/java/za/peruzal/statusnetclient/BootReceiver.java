package za.peruzal.statusnetclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Administrator on 8/7/13.
 */
public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, UpdaterService.class));
        Log.d(BootReceiver.class.getSimpleName(), "onReceived");
    }
}
