package za.peruzal.statusnetclient;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Build;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created by Administrator on 8/6/13.
 */
public class StatusData {
    private  static final  String TAG = DBHelper.class.getSimpleName();
    static final String DB_NAME = "timeline.db";
    static final int DB_VERSION = 4;
    static final String TABLE = "timeline";
    static final String C_ID = BaseColumns._ID;
    static final String C_CREATED_AT = "created_at";
    static final String C_SOURCE = "source";
    static final String C_TEXT = "txt";
    static final String C_USER = "user";

    private static final String GET_ALL_ORDER_BY  = C_CREATED_AT + " DESC";
    private static final String[] MAX_CREATED_AT_COLUMNS = { "MAX("+ StatusData.C_CREATED_AT +" )" };
    private static final String[] DB_TEXT_COLUMNS = { C_TEXT };

    Context context;
    private final DBHelper dbHelper;

    public StatusData(Context context) {
        this.dbHelper = new DBHelper(context);
        this.context = context;
        Log.i(TAG, "Initialized data");
    }



    //Close the database
    public void close()
    {
        this.dbHelper.close();
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    public void  insertOrIgnore(ContentValues values){
        Log.d(TAG, "insertOrignore on " + values);
        Uri uri = context.getContentResolver().insert(StatusProvider.CONTENT_URI,values);
        Log.d(TAG, uri.toString());
    }

    public Cursor getStatusUpdates(){
        return context.getContentResolver().query(StatusProvider.CONTENT_URI, null,null,null, GET_ALL_ORDER_BY);
    }

    //Return TimeStamp of the latest status we have in the database
    public long getLatestStatusCreatedAtTime(){
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();

        try {
            Cursor cursor = context.getContentResolver().query(StatusProvider.CONTENT_URI,MAX_CREATED_AT_COLUMNS, null,null,null);
            try {
                return cursor.moveToNext() ? cursor.getLong(0): Long.MIN_VALUE;
            } finally  {
                cursor.close();
            }
        }finally {
            db.close();
        }
    }

    public String getStatusByTextId(long id){
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();
        try {
            Cursor cursor = context.getContentResolver().query(StatusProvider.CONTENT_URI,DB_TEXT_COLUMNS,null,null,null);
            try {
                return cursor.moveToFirst() ? cursor.getString(0) : null;
            } finally {
                cursor.close();
            }
        } finally {
            db.close();
        }
    }

}
