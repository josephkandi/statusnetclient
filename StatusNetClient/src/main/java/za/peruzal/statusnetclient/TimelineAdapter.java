package za.peruzal.statusnetclient;

import android.content.Context;
import android.database.Cursor;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * Created by Administrator on 8/7/13.
 */
public class TimelineAdapter extends SimpleCursorAdapter {
    static final String[] FROM = { StatusData.C_CREATED_AT, StatusData.C_USER, StatusData.C_TEXT };
    static final int[] TO = { R.id.textCreatedAt, R.id.textUser, R.id.textText };

    public TimelineAdapter(Context context, Cursor c) {
        super(context, R.layout.row, c, FROM, TO);
    }

    @Override
    public void bindView(View row, Context context, Cursor cursor) {
        super.bindView(row, context, cursor);

        //Manually bind created at timestamp to its view
        long timestamp = cursor.getLong(cursor.getColumnIndex(StatusData.C_CREATED_AT));
        TextView textCreatedAt = (TextView)row.findViewById(R.id.textCreatedAt);
        textCreatedAt.setText(DateUtils.getRelativeTimeSpanString(timestamp));
    }
}
