package za.peruzal.statusnetclient;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import winterwell.jtwitter.Status;
import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;


public class StatusActivity extends BaseActivity implements OnClickListener, TextWatcher{
    public final static String TAG = StatusActivity.class.getSimpleName();

    EditText editText;
    Button updateButton;
    Twitter twitter;
    TextView textCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status);



        //Find views
        editText = (EditText)findViewById(R.id.editText);
        updateButton = (Button)findViewById(R.id.buttonUpdate);
        editText.addTextChangedListener(this);

        textCount = (TextView)findViewById(R.id.textCount);
        textCount.setText(Integer.toString(140));
        textCount.setTextColor(Color.GREEN);

        updateButton.setOnClickListener(this);

    }

    public void onClick(View view){
        String status = editText.getText().toString();
        new PostToTwitter().execute(status);
        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onClicked");
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void afterTextChanged(Editable statusText) {
        int count = 140 - statusText.length();
        textCount.setText(Integer.toString(count));
        textCount.setTextColor(Color.GREEN);

        if (count < 10)
            textCount.setTextColor(Color.YELLOW);
        if (count < 0)
            textCount.setTextColor(Color.RED);
    }

    //Asynchronoulsy post to twitter
    private class PostToTwitter extends AsyncTask<String, Long, String>
    {
        @Override
        protected void onProgressUpdate(Long... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                winterwell.jtwitter.Status status = ((StatusNetApplication)getApplication()).getTwitter().setStatus(strings[0]);

                return "Status updated to " + status.toString();
            }catch (TwitterException e){
                Log.e(TAG, e.toString());
                e.printStackTrace();

                return "Failed to post";
            } catch (Exception e){
                Log.e(TAG, e.toString());
                e.printStackTrace();

                return "Failed to post";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Context context = StatusActivity.this.getApplicationContext();
            //Get the manager
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);


            //Create the notificaction
            Notification updateComplete = new Notification();
            updateComplete.icon = android.R.drawable.stat_notify_sync;
            updateComplete.tickerText = result;
            updateComplete.when = System.currentTimeMillis();

            //Creating a Pending Intent
            Intent notificationIntent = new Intent(context, StatusActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(context,0,notificationIntent,0);


            updateComplete.setLatestEventInfo(context,"Status Net Update",result,contentIntent);

            //Show the notification
            notificationManager.notify(100, updateComplete);

            //Toast.makeText(StatusActivity.this,result, Toast.LENGTH_SHORT ).show();
        }
    }
    
}
