package za.peruzal.statusnetclient;

import android.os.AsyncTask;

import winterwell.jtwitter.Twitter;

/**
 * Created by Joseph on 2013/07/29.
 */
public class StatusUpdate extends AsyncTask<String, Void, String> {
   Twitter twitter;
    @Override
    protected String doInBackground(String... strings) {
        final String USERNAME = "student";
        final String PASSWORD = "password";
        final String API_ROOT_URL  = "http://yamba.marakana.com/api";
        twitter = new Twitter(USERNAME, PASSWORD);
        twitter.setAPIRootUrl(API_ROOT_URL);

        winterwell.jtwitter.Status status = twitter.setStatus(strings[0]);

        return status.toString();
    }
}
