package za.peruzal.statusnetclient;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.util.Log;

import java.util.List;

import winterwell.jtwitter.Status;
import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;

/**
 * Created by Administrator on 8/6/13.
 */
public class UpdaterService extends Service {
    static final String TAG = UpdaterService.class.getSimpleName();
    static final int DELAY = 60000; //a minute
    static final String NEW_STATUS_INTENT = "za.peruzal.statusnetclient.NEW_STATUS";
    static final String RECEIVE_TIMELINE_NOTIFICATIONS = "za.peruzal.statusnetclient.RECEIVE_TIMELINE_NOTIFICATIONS";
    private static final String NEW_STATUS_EXTRA_COUNT = "count";
    private boolean runFlag = false;
    private Updater updater;
    private StatusNetApplication app;
    Intent intent;

    SQLiteDatabase db;

    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.updater = new Updater();
        this.app = (StatusNetApplication)getApplication();


        Log.d(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       super.onStartCommand(intent, flags, startId);
       this.runFlag = true;
       this.updater.start();
       this.app.setServiceRunning(true);
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        this.runFlag = false;
        this.updater.interrupt();
        this.updater = null;
        this.app.setServiceRunning(false);

        Log.d(TAG, "onDestroy");
    }

    private class Updater extends Thread
    {

        private Updater() {
            super("UpdaterService-Updater");
        }

        @Override
        public void run() {
           UpdaterService updaterService = UpdaterService.this;
            while (updaterService.runFlag){
                Log.d(TAG, "Running background thread");
                try {
                    StatusNetApplication app = (StatusNetApplication)updaterService.getApplication();

                    int newUpdates = app.fetchStatusUpdates();
                    if (newUpdates > 0){
                        Log.d(TAG, "We have a new status");
                        intent = new Intent(NEW_STATUS_INTENT);
                        intent.putExtra(NEW_STATUS_EXTRA_COUNT, newUpdates);
                        Log.d(TAG, "Sent broadcast");
                        updaterService.sendBroadcast(intent, RECEIVE_TIMELINE_NOTIFICATIONS);
                    }
                    Thread.sleep(DELAY);
                }catch (InterruptedException e){
                    updaterService.runFlag = false;
                }
            }
        }
    }

}
