package za.peruzal.statusnetclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * Created by Administrator on 8/7/13.
 */
public class BaseActivity extends Activity {
    private static final String TAG = BaseActivity.class.getSimpleName();
    StatusNetApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (StatusNetApplication)getApplication();
        Log.d(TAG, "OnCreate with app  : " + app.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemStatus:
                startActivity(new Intent(this, StatusActivity.class));
                break;
            case R.id.itemPrefs:
                startActivity(new Intent(this, PrefsActivity.class).addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT));
                break;
            case R.id.itemToggleService:
                if (app.isServiceRunning()){
                    stopService(new Intent(this, UpdaterService.class));
                }else {
                    startService(new Intent(this, UpdaterService.class));
                }
                break;
            case R.id.itemPurge:
                //Delete all data
                Toast.makeText(this,"Deleted all data", Toast.LENGTH_LONG).show();
                break;
            case R.id.itemTimeline:
                startActivity(new Intent(this, TimelineActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                break;
        }
        return true;
    }

   @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem toggleitem = menu.findItem(R.id.itemToggleService);

        Log.d(TAG, "menu Opened " + menu.toString());

        if (app.isServiceRunning()){
            toggleitem.setTitle(R.string.title_service_stop);
            toggleitem.setIcon(android.R.drawable.ic_media_pause);
        }else{
            toggleitem.setTitle(R.string.title_service_start);
            toggleitem.setIcon(android.R.drawable.ic_media_play);
        }
        return true;
    }
}
