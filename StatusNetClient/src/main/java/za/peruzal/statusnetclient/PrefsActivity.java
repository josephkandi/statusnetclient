package za.peruzal.statusnetclient;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Joseph on 2013/07/30.
 */
public class PrefsActivity extends PreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.xml.prefs);
        addPreferencesFromResource(R.xml.prefs);
    }
}